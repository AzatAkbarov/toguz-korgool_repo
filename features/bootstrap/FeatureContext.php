<?php

use Behat\Mink\Exception\ElementNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext extends MainContext
{

    /**
     * @var Response|null
     */
    private $response;


    /**
     * @When /^я перехожу на страницу \'([^\']*)\'$/
     */
    public function яПерехожуНаСтраницу($arg1)
    {
        $this->visit($this->getContainer()->get('router')->generate($arg1));

    }

    /**
     * @When /^я заполняю поле \'([^\']*)\' значением \'([^\']*)\'$/
     * @throws ElementNotFoundException
     */
    public function яЗаполняюПолеЗначением($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^нажимаю кнопку \'([^\']*)\'$/
     * @throws ElementNotFoundException
     */
    public function нажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
    }

    /**
     * @When /^я вижу \'([^\']*)\' элемент \'([^\']*)\'$/
     */
    public function яВижуЭлемент($arg1, $arg2)
    {
        if($arg1 === 'text'){
            $arg1 = 'xpath';
            $arg2 = '//label[text()="' . $arg2 . '"]';
        }

        $this->getSession()->getPage()->find($arg1, $arg2);
    }

    /**
     * @BeforeScenario
     */
    public static function загружаюФикстуры()
    {
        $command = __DIR__ . '/../../bin/console doctrine:fixtures:load -n';
        $process = new Process($command);
        $process->run();
    }

    /**
     * @When /^я перехожу по ссылке \'([^\']*)\'$/
     * @throws ElementNotFoundException
     */
    public function яПерехожуПоСсылке($arg1)
    {
        $this->getSession()->getPage()->clickLink($arg1);
    }

    /**
     * @When /^я оказываюсь на странице \'([^\']*)\'  с параметром \'([^\']*)\' и значением \'([^\']*)\'$/
     */
    public function яОказываюсьНаСтраницеСПараметромИЗначением($arg1, $arg2, $arg3)
    {
        return $this->getContainer()->get('router')->generate($arg1, [$arg2 => $arg3]) === $this->getSession()->getCurrentUrl();
    }

}
