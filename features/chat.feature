#language: ru

Функционал: Проверяем функционал отправки сообщений в общий чат (прим. Выбранная локаль - RU)
  Сценарий: Проверяем отправку сообщений в общий чат
    Допустим я перехожу на страницу 'fos_user_security_login'
    И я вижу 'css' элемент 'form-signin'
    Тогда я заполняю поле '_username' значением 'test@gmail.com'
    И я заполняю поле '_password' значением 'root'
    И нажимаю кнопку '_submit'
    Тогда я вижу 'css' элемент 'form-message'
    И я заполняю поле 'form-message' значением 'Флюгегехаймен'
    И нажимаю кнопку 'form-submit'
    Тогда я вижу 'text' элемент 'Флюгегехаймен'

  Сценарий: Проверяем функционал отключения и включения звука в чате
    Допустим я перехожу на страницу 'fos_user_security_login'
    И я вижу 'css' элемент 'form-signin'
    Тогда я заполняю поле '_username' значением 'test@gmail.com'
    И я заполняю поле '_password' значением 'root'
    И нажимаю кнопку '_submit'
    Тогда я вижу 'css' элемент 'fa-volume-up'
    И нажимаю кнопку 'soundMessageBtn'
    Тогда я вижу 'css' элемент 'fa-volume-off'
    И нажимаю кнопку 'soundMessageBtn'
    Тогда я вижу 'css' элемент 'fa-volume-up'

  Сценарий: Проверяем работоспособность кнопки перехода в общую комнату чата
    Допустим я перехожу на страницу 'fos_user_security_login'
    И я вижу 'css' элемент 'form-signin'
    Тогда я заполняю поле '_username' значением 'test@gmail.com'
    И я заполняю поле '_password' значением 'root'
    И нажимаю кнопку '_submit'
    Тогда я вижу 'css' элемент 'sendToRoomAll'
    И нажимаю кнопку 'sendToRoomAll'
    Тогда я вижу 'text' элемент 'Общий чат'