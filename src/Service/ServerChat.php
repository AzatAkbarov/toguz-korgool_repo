<?php

namespace App\Service;

use App\CustomLogger\CustomLogger;
use App\Handler\ChatHandler;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServerChat implements MessageComponentInterface
{
    /** @var ContainerInterface $container */
    protected $container;
    /** @var ChatHandler $handler */
    protected $handler;

    /**
     * ServerChat constructor.
     * @param ContainerInterface $container
     * @param ChatHandler $handler
     */
    public function __construct(ContainerInterface $container, ChatHandler $handler)
    {
        $this->container = $container;
        $this->handler = $handler;
    }

    /**
     * @param ConnectionInterface $conn
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function onOpen(ConnectionInterface $conn)
    {
        parse_str($conn->httpRequest->getUri()->getQuery(), $queryParameters);
        $token = $queryParameters['token'];
        $user = $this->handler->getUserByToken($token, $conn);
        if ($user) {
            $this->handler->addToAuthorized($user,$conn);
            $this->handler->attachClient($conn);
            $this->handler->sendListOnlineClients();
            return;
        }
        $this->handler->disconnectClient($conn);
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $this->handler->messageProcessing($msg, $from);
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->handler->detachClient($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->handler->detachClient($conn);
        $this->handler->disconnectClient($conn);
        CustomLogger::_logNotice("An error has occurred: {$e->getMessage()}", self::class);
    }
}
