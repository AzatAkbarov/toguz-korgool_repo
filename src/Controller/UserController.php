<?php


namespace App\Controller;



use App\Entity\Gamelog;
use App\Entity\User;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Handler\UserHandler\UserHandler;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/profile/{username}", name="user_profile")
     * @ParamConverter("user", class="App\Entity\User")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userProfileAction(User $user, UserHandler $userHandler)
    {
        $doctrine = $this->getDoctrine()->getRepository(Gamelog::class);
        $userStatisticsArray = $userHandler->dqlQueryByUserStatistics($doctrine, $user);
        $totalPoint = $userStatisticsArray['totalPointWhite'] + $userStatisticsArray['totalPointBlack'];
        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'victory' => $userStatisticsArray['victory'],
            'loss' => $userStatisticsArray['loss'],
            'deadHeat' => $userStatisticsArray['deadHeat'],
            'allGames' => $userStatisticsArray['allGames'],
            'whiteSideWherePlayerWin' => $userStatisticsArray['whiteSideWherePlayerWin'],
            'blackSideWherePlayerWin' => $userStatisticsArray['blackSideWherePlayerWin'],
            'whiteSideWherePlayerLoss' => $userStatisticsArray['whiteSideWherePlayerLoss'],
            'blackSideWherePlayerLoss' => $userStatisticsArray['blackSideWherePlayerLoss'],
            'whiteSideWherePlayerDeadHead' => $userStatisticsArray['whiteSideWherePlayerDeadHead'],
            'blackSideWherePlayerDeadHead' => $userStatisticsArray['blackSideWherePlayerDeadHead'],
            'totalPoint' => $totalPoint,
            'totalPointWhite' => $userStatisticsArray['totalPointWhite'],
            'totalPointBlack' => $userStatisticsArray['totalPointBlack'],
        ]);
    }

    /**
     * @Route("/edit", name="user_edit")
     * @param Request $request
     * @param UserManagerInterface $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function userEditAction(Request $request, UserManagerInterface $manager, UserHandler $userHandler)
    {
        /** @var User $user */
        $user = $this->getUser();
        $oldAvatar = $user->getAvatar();
        $user->setAvatar(null);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($request->isXMLHttpRequest()) {
            $avatarBase64 = $request->get('avatar');
            $fullName = $request->get('fullName');
            if ($avatarBase64){
                $newAvatar = $userHandler->avatarHandler($avatarBase64, $oldAvatar);
                $user->setAvatar($newAvatar);
            } else{
                $user->setAvatar($oldAvatar);
            }
            $user->setFullName($fullName);
            $manager->updateUser($user);
        }
        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
            'avatar' => $oldAvatar
        ]);
    }
}