<?php

namespace App\Controller;

use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends SecurityController
{
    /**
     * @Route("/login")
     */
    public function ourLoginAction(Request $request)
    {
        if($this->getUser()){
            return $this->redirectToRoute('fos_user_security_logout');
        }
        else {
            return parent::loginAction($request);
        }
    }
}