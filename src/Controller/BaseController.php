<?php


namespace App\Controller;

use App\Entity\Content;
use App\Entity\Gamelog;
use App\Entity\User;
use App\Repository\GamelogRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends Controller
{
    /**
     * @Route("/", name="index_action")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/content/{id}", name="content")
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("content", class="App\Entity\Content")
     * @param Content $content
     *
     */
    public function contentAction(Content $content)
    {
        return $this->render('content.html.twig', ['content' => $content]);
    }

    public function recentCategories(ObjectManager $manager)
    {
        $categories = $manager->getRepository('App:Category')->findAll();
        return $this->render('categories/recent_list.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/history_game", name="app_history_game")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function gamelogAction(Request $request)
    {
        $gamelogRepository = $this->getDoctrine()->getRepository(Gamelog::class);

        $numbersRowsInGamelog = $gamelogRepository->countAllRowsInGameLog();

        $pages = [];
        $offset = $request->request->get('offset') ?? 0;
        $qty = $request->request->get('qty') ?? 10;
        $qtyPages = number_format(ceil($numbersRowsInGamelog / $qty), 0);

        for ($i = 1, $y = 0; $i <= $qtyPages; $i++, $y++) {
            $pages[$y]['page'] = $i;
            if ($i == (int)$offset + 1) {
                $pages[$y]['currentPage'] = $i;
            }
        }

        $offset = $offset * ((int)$qtyPages === 1 ? 0 : $qty);
        $data = $gamelogRepository->getGamelogsByOffset($offset, $qty);


        return $this->render('history_game/history_game.html.twig', [
            'gamelogs' => $data,
            'pages' => $pages ?? null,
            'qty' => $qty ?? null
        ]);
    }

    /**
     * @Route("/user_history_game/{username}", name="user_history_game", methods="GET|POST")
     * @ParamConverter("user", class="App\Entity\User")
     * @param User $user
     * @param ObjectManager $em
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showUserHistoryGame(User $user, ObjectManager $em, Request $request)
    {
        /** @var GamelogRepository $gamelogRepository */
        $gamelogRepository =  $em->getRepository(Gamelog::class);
        $qtyRaws = $gamelogRepository->countAllRowsInGameLogByUser($user);
        dump($qtyRaws);

        $pages = [];
        $offset = $request->request->get('offset') ?? 0;
        $qty = $request->request->get('qty') ?? 10;
        $qtyPages = number_format(ceil($qtyRaws / $qty), 0);

        for ($i = 1, $y = 0; $i <= $qtyPages; $i++, $y++) {
            $pages[$y]['page'] = $i;
            if ($i == (int)$offset + 1) {
                $pages[$y]['currentPage'] = $i;
            }
        }

        $offset = $offset * ((int)$qtyPages === 1 ? 0 : $qty);

        $data = $gamelogRepository->getUserGamelogsByOffset($offset, $qty, $user);

        return $this->render('history_game/history_game.html.twig', [
            'gamelogs' => $data,
            'pages' => $pages ?? null,
            'qty' => $qty ?? null,
            'player' => $user
        ]);
    }


    /**
     * @Route("/show_history_game/{id}", name="app_show_history_game")
     * @param Gamelog $gamelog
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showHistoryGameAction(Gamelog $gamelog)
    {
        return $this->render('history_game/show_history_game.html.twig', ['gamelog' => $gamelog]);
    }
}