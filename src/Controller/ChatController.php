<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ChatController extends Controller
{
    /**
     * @Route("/chat", name="app_chat")
     */
    public function chatAction()
    {
        $portChat = $this->container->getParameter('server_chat')['port'];
        $ipChat = $this->container->getParameter('server_chat')['ip'];
        $portGame = $this->container->getParameter('server_game')['port'];
        $ipGame = $this->container->getParameter('server_game')['ip'];

        return $this->render('chat/chat.html.twig', [
            'urlChat' => "ws://{$ipChat}:{$portChat}",
            'urlGame' => "ws://{$ipGame}:{$portGame}"
        ]);
    }
}
