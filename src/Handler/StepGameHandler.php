<?php
/**
 * Created by PhpStorm.
 * User: kremnevea
 * Date: 15.09.18
 * Time: 13:48
 */

namespace App\Handler;


class StepGameHandler
{
    /** @var array $stateGames */
    protected $stateGames = [];

    const BASE_STATE_GAME = [
        'WhiteCells' => [9, 9, 9, 9, 9, 9, 9, 9, 9],
        'BlackCells' => [9, 9, 9, 9, 9, 9, 9, 9, 9],
        'WhiteSalt' => 0,
        'BlackSalt' => 0,
        'WhiteCauldron' => 0,
        'BlackCauldron' => 0
    ];

    protected const BASE_STEP_DATA = [
        'TakeBalls' => [],
        'DropBallsToCells' => [],
        'DropBallsToCauldron' => [],
        'PlaceSalt' => [],
        'ExecutionOrder' => []
    ];

    /**
     * @param string $keyRoom
     * @param int $cell
     * @param string $sideColor
     * @param string $opponentColor
     * @return array
     */
    public function getDataStepGame(string $keyRoom, int $cell, string $sideColor, string $opponentColor)
    {
        $currentState = $this->stateGames[$keyRoom]['current'];
        $amountBallsInCell = $currentState["{$sideColor}Cells"][$cell - 1];
        $currentState["{$sideColor}Cells"][$cell - 1] = 0;
        $opponentSaltCell = $currentState["{$opponentColor}Salt"];
        $saltCell = $currentState["{$sideColor}Salt"];
        $dropBallsToCells = [];
        $lastSideDropBall = $sideColor;
        $addToCell = $cell;
        $isSalt = false;
        $isBallInSaltCell = false;
        $isBallInOpponentSaltCell = false;
        $dataIfIsAtSyroo = null;

        if ($amountBallsInCell === 1) $addToCell++;

        $stepData = $this->getTakeBalls(self::BASE_STEP_DATA, $cell, $sideColor, $amountBallsInCell);

        for ($i = 0; $i < $amountBallsInCell; $i++) {
            if ($addToCell > 9) {
                $addToCell = 1;
                if ($lastSideDropBall === 'White') $lastSideDropBall = 'Black';
                else $lastSideDropBall = 'White';
            }

            $dropBallsToCells[] = ['Number' => $addToCell, 'BoardSide' => $lastSideDropBall, 'Amount' => 1];
            $currentState["{$lastSideDropBall}Cells"][$addToCell - 1]++;

            if ($addToCell === $saltCell && $lastSideDropBall === $opponentColor) {
                $currentState["{$opponentColor}Cells"][$addToCell - 1] = 0;
                $currentState["{$sideColor}Cauldron"]++;
                $isBallInSaltCell = true;
            }

            if ($addToCell === $opponentSaltCell && $lastSideDropBall === $sideColor) {
                $currentState["{$sideColor}Cells"][$addToCell - 1] = 0;
                $currentState["{$opponentColor}Cauldron"]++;
                $isBallInOpponentSaltCell = true;
            }

            $addToCell++;
        }

        if ($addToCell > 1) $addToCell--;

        $stepData = $this->getDropBallsToCells($stepData, $dropBallsToCells);
        $amountBallInLastCell = $currentState["{$lastSideDropBall}Cells"][$addToCell - 1];

        if ($lastSideDropBall === $opponentColor && $addToCell !== 9 && $addToCell !== $opponentSaltCell
            && $amountBallInLastCell === 3 && $saltCell === 0) {
            $currentState["{$sideColor}Salt"] = $addToCell;
            $isSalt = true;
        }

        if ($amountBallInLastCell % 2 === 0 && $lastSideDropBall === $opponentColor || $isSalt) {
            $currentState["{$opponentColor}Cells"][$addToCell - 1] = 0;
            $currentState["{$sideColor}Cauldron"] += $amountBallInLastCell;
            $stepData = $this->getTakeBalls($stepData, $addToCell, $opponentColor, $amountBallInLastCell);
            $stepData = $this->getDropBallsToCauldron($stepData, $sideColor, $amountBallInLastCell);
        }

        if ($isBallInSaltCell) {
            $stepData = $this->getTakeBalls($stepData, $saltCell, $opponentColor);
            $stepData = $this->getDropBallsToCauldron($stepData, $sideColor);
        }

        if ($isBallInOpponentSaltCell) {
            $stepData = $this->getTakeBalls($stepData, $opponentSaltCell, $sideColor);
            $stepData = $this->getDropBallsToCauldron($stepData, $opponentColor);
        }

        if ($isSalt) $stepData = $this->getPlaceSalt($stepData, $addToCell, $lastSideDropBall);

        if (!array_sum($currentState["{$opponentColor}Cells"])) {
            $this->stateGames[$keyRoom]["AtSyroo{$opponentColor}"][] = true;
            if($this->isAtSyroo($keyRoom,$opponentColor)) {
                $dataIfIsAtSyroo = $this->getAtSyrooData($sideColor, $stepData, $currentState);
            }
        }

        if (!array_sum($currentState["{$sideColor}Cells"])) {
            $this->stateGames[$keyRoom]["AtSyroo{$sideColor}"][] = true;
            if($this->isAtSyroo($keyRoom,$sideColor)) {
                $dataIfIsAtSyroo = $this->getAtSyrooData($opponentColor, $stepData, $currentState);
            }
        }

        $this->stateGames[$keyRoom]['current'] = $dataIfIsAtSyroo ? $dataIfIsAtSyroo['currentState'] : $currentState;
        $this->stateGames[$keyRoom]['steps'][] = $dataIfIsAtSyroo ? $dataIfIsAtSyroo['stepData'] : $stepData;
        return $dataIfIsAtSyroo ? $dataIfIsAtSyroo['stepData'] : $stepData;
    }

    public function getAtSyrooData(string $sideColor, array $stepData, array $currentState){
        for ($i = 0; $i < 9; $i++) {
            $amount = $currentState["{$sideColor}Cells"][$i];
            if ($amount > 0) {
                $currentState["{$sideColor}Cells"][$i] = 0;
                $currentState["{$sideColor}Cauldron"] += $amount;
                $stepData = $this->getTakeBalls($stepData, $i + 1, $sideColor);
                $stepData = $this->getDropBallsToCauldron($stepData, $sideColor, $amount);
            }
        }

        return ['stepData' => $stepData, 'currentState' => $currentState];
    }

    /**
     * @param array $stepData
     * @param string $sideColor
     * @param int $amount
     * @return array
     */
    public function getDropBallsToCauldron(array $stepData, string $sideColor, int $amount = 1)
    {
        $stepData['DropBallsToCauldron'][] = [
            'Identifier' => 'DropBallsToCauldron' . (count($stepData['DropBallsToCauldron']) + 1),
            'Data' => [
                'IsFromHandful' => true,
                'BoardSide' => $sideColor,
                'Amount' => $amount
            ]
        ];
        $stepData['ExecutionOrder'][] = 'DropBallsToCauldron' . count($stepData['DropBallsToCauldron']);
        return $stepData;
    }

    /**
     * @param array $stepData
     * @param int $cell
     * @param string $sideColor
     * @param int $amount
     * @return array
     */
    public function getTakeBalls(array $stepData, int $cell, string $sideColor, int $amount = 1)
    {
        $stepData['TakeBalls'][] = [
            'Identifier' => 'TakeBalls' . (count($stepData['TakeBalls']) + 1),
            'Data' => [
                'FromCell' => ['Number' => $cell, 'BoardSide' => $sideColor],
                'Amount' => $amount
            ]
        ];
        $stepData['ExecutionOrder'][] = 'TakeBalls' . (count($stepData['TakeBalls']));
        return $stepData;
    }

    /**
     * @param array $stepData
     * @param array $dropBallsToCells
     * @return array
     */
    public function getDropBallsToCells(array $stepData, array $dropBallsToCells)
    {
        $stepData['DropBallsToCells'][] = [
            'Identifier' => 'DropBallsToCells1',
            'Data' => [
                'IsFromHandful' => true,
                'Cells' => $dropBallsToCells
            ]
        ];
        $stepData['ExecutionOrder'][] = 'DropBallsToCells1';
        return $stepData;
    }

    /**
     * @param array $stepData
     * @param int $cell
     * @param string $sideColor
     * @return array
     */
    public function getPlaceSalt(array $stepData, int $cell, string $sideColor)
    {
        $stepData['PlaceSalt'][] = [
            'Identifier' => 'PlaceSalt1',
            'Data' => [
                'ToCell' => ['Number' => $cell, 'BoardSide' => $sideColor]
            ],
        ];
        $stepData['ExecutionOrder'][] = 'PlaceSalt1';
        return $stepData;
    }

    /**
     * @param string $keyRoom
     * @param string $sideColor
     */
    public function setSideTurn(string $keyRoom, string $sideColor)
    {
        $this->stateGames[$keyRoom]['sideTurn'] = $sideColor;
    }

    /**
     * @param string $keyRoom
     * @return mixed
     */
    public function getSideTurn(string $keyRoom)
    {
        return $this->stateGames[$keyRoom]['sideTurn'];
    }

    /**
     * @param string $keyRoom
     * @return bool
     */
    public function isStepsData(string $keyRoom)
    {
        return isset($this->stateGames[$keyRoom]);
    }

    /**
     * @param string $keyRoom
     */
    public function setBaseGameState(string $keyRoom)
    {
        $this->stateGames[$keyRoom] = ['current' => self::BASE_STATE_GAME, 'sideTurn' => 'White'];
    }

    /**
     * @param string $keyRoom
     * @param int $cell
     * @param string $sideColor
     * @return bool
     */
    public function isNotEmptyCellAndNotSalt(string $keyRoom, int $cell, string $sideColor)
    {
        if ($this->stateGames[$keyRoom]['current']["{$sideColor}Cells"][$cell - 1] > 0) return true;
        return false;
    }

    /**
     * @param string $keyRoom
     * @param string $sideColor
     * @return mixed
     */
    public function getPointInCauldron(string $keyRoom, string $sideColor)
    {
        return $this->stateGames[$keyRoom]['current']["{$sideColor}Cauldron"];
    }

    /**
     * @param string $keyRoom
     * @param string $sideColor
     * @return bool
     */
    public function isTurnSide(string $keyRoom, string $sideColor)
    {
        if ($sideColor === $this->stateGames[$keyRoom]['sideTurn']) return true;
        return false;
    }

    /**
     * @param string $keyRoom
     * @param string $sideColor
     * @return bool
     */
    public function isAtSyroo(string $keyRoom, string $sideColor){
        return isset($this->stateGames[$keyRoom]["AtSyroo{$sideColor}"][1]);
    }

    /**
     * @param string $keyRoom
     * @return mixed
     */
    public function getGameLogStepsData(string $keyRoom){
        return $this->stateGames[$keyRoom]['steps'];
    }

    /**
     * @param string $keyRoom
     */
    public function clearGameData(string $keyRoom){
        unset($this->stateGames[$keyRoom]);
    }
}