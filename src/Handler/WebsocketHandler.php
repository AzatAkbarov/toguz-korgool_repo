<?php
/**
 * Created by PhpStorm.
 * User: kremnevea
 * Date: 07.09.18
 * Time: 14:09
 */

namespace App\Handler;

use App\Entity\User;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use App\CustomLogger\CustomLogger;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebsocketHandler implements WebsocketHandlerInterface
{
    /** @var ContainerInterface $container */
    protected $container;
    /** @var array $rooms */
    protected $rooms = [];
    protected $redis;
    /** @var \Doctrine\ORM\EntityManager|object $entityManager */
    protected $entityManager;
    const REDIS_DSN = 'redis://localhost';
    const REDIS_GAME_ROOMS_HASH = 'game_rooms';
    const REDIS_CONNECTIONS_CHAT_HASH = 'connections_chat';
    const REDIS_ONLINE_USERS_HASH = 'online_users';

    /**
     * AbstractWebsocketHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $this->redis = RedisAdapter::createConnection(self::REDIS_DSN);
    }

    /**
     * @param string $token
     * @param string $keyRoom
     * @param ConnectionInterface $conn
     * @return mixed
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function getUserByToken(string $token, ConnectionInterface $conn, string $keyRoom = null){
        if (iconv_strlen($token) !== 43) {
            CustomLogger::_logNotice("Token {$token} is not valid", self::class);
            return false;
        }
        $this->entityManager->clear();

        /** @var mixed $user */
        $user = $this->entityManager->getRepository(User::class)->findByToken($token);

        if($user) return $user;

        CustomLogger::_logNotice("User by token {$token} not found", self::class);
        return false;
    }

    /**
     * @param ConnectionInterface $conn
     * @return void
     */
    public function disconnectClient(ConnectionInterface $conn){
        CustomLogger::_logNotice("Connection {$conn->resourceId} has disconnected", self::class);
        $conn->close();
    }
}