<?php

namespace App\Handler;


class EnumeratingChatMessages
{
    const MSG_IN_GAME = [
        'type' => 'service',
        'sub_type' => 'in_game',
        'message' => '',
        'from' => 'server',
        'clientUpdate' => [
            'username' => '',
            'clientData' => '',
        ]
    ];

    const MSG_NOTICE = [
        'type' => 'service',
        'sub_type' => 'notice',
        'message' => '',
        'from' => 'server',
        'clientUpdate' => [
            'username' => '',
            'clientData' => '',
        ]
    ];

    const MSG_BAN_ERROR = [
        'type' => 'service',
        'sub_type' => 'ban',
        'status' => "failed",
        'message' => '',
        'from' => 'server'
    ];

    static function getMsgAccessDenied()
    {
        return json_encode([
            'type' => 'service',
            'sub_type' => 'ban',
            'status' => "failed",
            'message' => 'no rights',
            'from' => 'server'
        ]);
    }

    static function getMsgWrongData()
    {
        return json_encode([
            'type' => 'service',
            'sub_type' => 'ban',
            'status' => "failed",
            'message' => 'wrong data',
            'from' => 'server'
        ]);
    }

    static function getMsgAlreadyBanned()
    {
        return json_encode([
            'type' => 'service',
            'sub_type' => 'ban',
            'status' => 'failed',
            'message' => "already banned",
            'from' => 'server'
        ]);
    }

    static function getMsgBanSuccess()
    {
        return json_encode([
            'type' => 'service',
            'sub_type' => 'ban',
            'status' => "success",
            'from' => 'server'
        ]);
    }

}