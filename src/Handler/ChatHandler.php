<?php

namespace App\Handler;

use App\CustomLogger\CustomLogger;
use App\Entity\Ban;
use App\Entity\User;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChatHandler extends WebsocketHandler
{
    /** @var \SplObjectStorage $clients */
    protected $clients;

    /**
     * ChatHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->clients = new \SplObjectStorage;
    }

    /**
     * @param User $user
     * @param ConnectionInterface $conn
     */
    public function addToAuthorized(User $user, ConnectionInterface $conn){
        CustomLogger::_logNotice(
            "Authorization {{$conn->resourceId} => {$user->getUsername()}} was successful",
            self::class
        );

        $this->closeEarlyConnectionByUser($user);
        $this->saveClient($user->getUsername(), $conn);
        $this->saveUserInOnLineList($conn, $user);
    }

    /**
     * @param User $user
     * @param ConnectionInterface $conn
     * @return bool
     */
    public function checkLastBan(User $user, ConnectionInterface $conn)
    {
        $repository = $this->entityManager->getRepository(Ban::class);
        /** @var Ban $userBan */
        $userBan = $repository->findLastBanByUserId($user->getId());
        $current_date = new \DateTime();
        if ($userBan && ($userBan->getEndDate() >= $current_date)) {
            $date = new \DateTime();
            $diff = ($userBan->getEndDate()->getTimestamp()) - ($date->getTimestamp());
            $this->sendNotificationBan($userBan, $conn, $diff);
            return true;
        }
        return false;
    }

    /**
     * @param Ban $ban
     * @param ConnectionInterface $conn
     * @param $diff
     */
    public function sendNotificationBan(Ban $ban, ConnectionInterface $conn, $diff)
    {
        $conn->send((json_encode([
            'type' => 'ban',
            'duration' => $diff,
            'description' => $ban->getDescription(),
            'from' => 'server'
        ])));
    }

    /**
     * @param string $username
     * @param ConnectionInterface $conn
     */
    public function saveClient(string $username, ConnectionInterface $conn)
    {
        $this->redis->hset(self::REDIS_CONNECTIONS_CHAT_HASH, $conn->resourceId, $username);

        $conn->send(json_encode([
            'type' => 'service',
            'message' => 'success',
            'from' => 'server'
        ]));
        CustomLogger::_logNotice("Client {$username} saved", self::class);
    }

    /**
     * @param string $msg
     * @param ConnectionInterface $conn
     */
    public function messageProcessing(string $msg, ConnectionInterface $conn)
    {
        $msg = json_decode($msg, true);
        $username = $this->redis->hget(self::REDIS_CONNECTIONS_CHAT_HASH, $conn->resourceId);

        if ($msg['type'] === 'message') {
            $destination = $msg['destination'];
            $message = $msg['message'];
            $dataForSend = json_encode([
                'type' => 'message',
                'message' => htmlspecialchars($message, ENT_QUOTES),
                'from' => $username,
                'destination' => $destination
            ]);
            list($key, $value) = explode("@", $destination);
            $arrayOfConns = $this->redis->hgetall(self::REDIS_CONNECTIONS_CHAT_HASH);
            $arrayOfConns = array_flip($arrayOfConns);
            if ($key == 'room') {
                $this->sendMessageInRoom($dataForSend, $arrayOfConns, $value);
            } else if ($key == 'user') {
                $this->sendPrivateMessage($dataForSend, $value, $conn, $arrayOfConns);
            }
        } else if ($msg['type'] === 'ban' || $msg['type'] === 'unBan') {
            $this->banUser($msg, $username, $conn);
        } else if ($msg['type'] === 'invite'){
            $this->inviteAndPrepareToGame($username, $msg, $conn);
        } else if ($msg['type'] === 'inviteAnswer'){
            $this->startGame($username, $msg);
        } else if ($msg['type'] === 'abortInvite'){
            $this->abortInviteAndRemovePrepared($username, $msg);
        } else if($msg['type'] === 'endGame'){
            $this->updateUsersAfterEndGame($msg);
        } else {
            $this->disconnectClient($conn);
        }
    }

    /**
     * @param $msg
     */
    private function updateUsersAfterEndGame($msg)
    {
        $usersName = $msg['keyRoom'];
        $userArray = explode('|', $usersName);
        if (isset($this->rooms['game_rooms'][$usersName]))
        {
            unset($this->rooms['game_rooms'][$usersName]);
            $this->updateOnlineUserListInClient($userArray[0], false);
            $this->updateOnlineUserListInClient($userArray[1], false);
        } else {
            return;
        }
    }

    /**
     * @param string $username
     * @param bool $isPlayed
     * @return mixed
     */
    public function updateOnlineUserInRedis(string $username, bool $isPlayed)
    {
        $getUserDataFromRedis = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $username);
        if ($getUserDataFromRedis){
            $arrayDataUser = json_decode($getUserDataFromRedis, true);
            $arrayDataUser['is_played'] = $isPlayed;
            $dataToRedis = json_encode($arrayDataUser);
            $this->redis->hset(self::REDIS_ONLINE_USERS_HASH, $username, $dataToRedis);
            return $arrayDataUser;
        } else {
            return null;
        }

    }

    /**
     * @param string $username
     * @param bool $isPlayed
     */
    public function updateOnlineUserListInClient(string $username, bool $isPlayed)
    {
        $userDataForSend = $this->updateOnlineUserInRedis($username, $isPlayed);
        if ($userDataForSend){
            $msgAll = json_encode(array_merge(EnumeratingChatMessages::MSG_IN_GAME, [
                'clientUpdate' => [
                    'username' => $username,
                    'clientData' => $userDataForSend,
                ]
            ]));
            $this->sendAll($msgAll);
        } else {
            return;
        }
    }

    /**
     * @param $isPlayerInGame
     * @return bool
     */
    public function isPlayerInGame($isPlayerInGame)
    {
        $allDataFromRedisGameRooms = $this->redis->hgetall(self::REDIS_GAME_ROOMS_HASH);
        if(!empty($allDataFromRedisGameRooms)){
            $arrTitleGameRooms = array_keys($allDataFromRedisGameRooms);
            foreach ($arrTitleGameRooms as $val){
                $user[] = explode('|', $val);
            }
            foreach ($user as $key => $array){
                if(in_array($isPlayerInGame, $array)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param string $username
     * @param array $msg
     */
    public function inviteAndPrepareToGame(string $username, array $msg, ConnectionInterface $conn)
    {
        $invitedUsername = $msg['to_username'];
        if($this->isPlayerInGame($username) || $this->isPlayerInGame($invitedUsername)){
            $conn->send(json_encode(['type' => 'invite_fail']));
            return;
        }
        $timeout = $msg['timeout'];
        $dataByUsername = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $invitedUsername);
        $gotData = json_decode($dataByUsername,true);
        $idConnInvited = $this->findUserConnect($gotData['id_conn']);
        $idConnInvited->send(json_encode([
            'type' => 'invite',
            'from_username' => $username,
            'timeout' => $timeout,
        ]));
        $this->rooms['game_rooms'][$username.'|'.$invitedUsername][$username] = $username;
        $this->rooms['game_rooms'][$username.'|'.$invitedUsername][$invitedUsername] = $invitedUsername;
        $this->updateOnlineUserListInClient($username, true);
        $this->updateOnlineUserListInClient($invitedUsername, true);
    }

    /**
     * @param $username
     * @return null|object
     */
    public function findConnectionByUserName($username)
    {
        $userDataFromRedis = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $username);
        if($userDataFromRedis){
            $arrData = json_decode($userDataFromRedis, true);
            return $this->findUserConnect($arrData['id_conn']);
        }
        return null;
    }

    /**
     * @param string $userNameWhoAgreeToPlay
     * @param array $msg
     */
    public function startGame(string $userNameWhoAgreeToPlay, array $msg)
    {
        $playerOneUserName = $msg['to_username'];
        $playerTwoUserName = $userNameWhoAgreeToPlay;

        $keyRoom = $playerOneUserName.'|'.$playerTwoUserName;

        $colors = ['White', 'Black'];
        $randColor = $colors[rand(0,1)];
        unset($colors[array_search($randColor, $colors)]);
        $colors = array_values($colors);

        $dataForRecordInRedis = json_encode(
            [
            $playerOneUserName => ['side_color' => $randColor],
            $playerTwoUserName => ['side_color' => $colors[0]]
            ]
        );

        $this->redis->hset(self::REDIS_GAME_ROOMS_HASH, $keyRoom, $dataForRecordInRedis);

        $playerOneConnection = $this->findConnectionByUserName($playerOneUserName);
        $playerTwoConnection = $this->findConnectionByUserName($playerTwoUserName);
        $playerOneConnection->send(
            json_encode([
                'type' => 'start_game',
                'side_color' => $randColor,
                'opponent' => $playerTwoUserName,
                'game_room_id' => $keyRoom
            ])
        );
        $playerTwoConnection->send(
            json_encode([
                'type' => 'start_game',
                'side_color' => $colors[0],
                'opponent' => $playerOneUserName,
                'game_room_id' => $keyRoom
            ])
        );
    }
    /**
     * @param string $userNameWhoDidAbortInvite
     * @param array $msg
     */
    public function abortInviteAndRemovePrepared(string $userNameWhoDidAbortInvite, array $msg)
    {
        $userNameWhoDidInvite = $msg['to_username'];
        $fullName = null;
        $userDataString = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $userNameWhoDidAbortInvite);
        if($userDataString){
            $userDataArray = json_decode($userDataString, true);
            $fullName = $userDataArray['showName'];
        }
        $connection = $this->findConnectionByUserName($userNameWhoDidInvite);
        if($connection){
            $msg = json_encode([
                    'type' => 'abort_invite',
                    'from' => $fullName,
            ]);
            $connection->send($msg);
        }
        $titleGameRoom = $userNameWhoDidInvite . '|' . $userNameWhoDidAbortInvite;
        unset($this->rooms['game_rooms'][$titleGameRoom]);
        $this->updateOnlineUserListInClient($userNameWhoDidAbortInvite, false);
        $this->updateOnlineUserListInClient($userNameWhoDidInvite, false);
    }
    /**
     * @param $dataForSend
     * @param $value
     * @param $conn
     * @param $arrayOfConns
     */
    public function sendPrivateMessage($dataForSend, $value, ConnectionInterface $conn, $arrayOfConns)
    {
        $id_conn = $arrayOfConns[$value];
        foreach ($this->clients as $client) {
            if ($client->resourceId == $id_conn) {
                $client->send($dataForSend);
                break;
            }
        }
        $conn->send($dataForSend);
    }

    /**
     * @param $dataForSend
     * @param $arrayOfConns
     * @param $roomName
     */
    public function sendMessageInRoom($dataForSend, $arrayOfConns, $roomName)
    {
        if ($roomName == 'all') {
            foreach ($this->clients as $client) {
                $client->send($dataForSend);
            }
        } else {
            $arrayOfUserInRoom = $this->rooms[$roomName];
            foreach ($arrayOfUserInRoom as $user) {
                if (isset($arrayOfConns[$user])) {
                    $id_conn = $arrayOfConns[$user];
                    foreach ($this->clients as $client) {
                        if ($client->resourceId == $id_conn) {
                            $client->send($dataForSend);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param ConnectionInterface $conn
     * @param User $user
     */
    public function saveUserInOnLineList(ConnectionInterface $conn, User $user)
    {
        $dataForSave = json_encode([
            'avatar' =>
                $user->getAvatar() ?
                    '/images/avatars/' . $user->getAvatar() :
                    '/assets/images/default/avatar_default.png',
            'showName' => $user->getFullName() ?? $user->getUsername(),
            'id_conn' => $conn->resourceId,
            'moderator' => $user->hasRole('ROLE_MODERATOR'),
            'banned' => $this->checkLastBan($user, $conn),
            'is_played' => false
        ], JSON_UNESCAPED_UNICODE | JSON_HEX_AMP);
        $this->redis->hset(self::REDIS_ONLINE_USERS_HASH, $user->getUsername(), $dataForSave);
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function attachClient(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function detachClient(ConnectionInterface $conn)
    {
        $username = $this->redis->hget(self::REDIS_CONNECTIONS_CHAT_HASH, $conn->resourceId);
        $this->redis->hdel(self::REDIS_CONNECTIONS_CHAT_HASH, $conn->resourceId);
        $this->redis->hdel(self::REDIS_ONLINE_USERS_HASH, $username);
        $this->clients->detach($conn);
        CustomLogger::_logNotice(
            "Connection {$conn->resourceId} with username {$username} was detached",
            self::class
        );
        $this->sendListOnlineClients();
    }

    public function sendListOnlineClients()
    {
        $dataForSend = json_encode([
            'type' => 'service',
            'onlineClients' => $this->redis->hgetall(self::REDIS_ONLINE_USERS_HASH),
            'message' => 'onlineClients',
            'from' => 'server'
        ]);
        foreach ($this->clients as $client) {
            $client->send($dataForSend);
        }
    }

    /**
     * @param User $user
     */
    public function closeEarlyConnectionByUser(User $user)
    {
        $dataFromRedis = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $user->getUsername());
        if ($dataFromRedis) {
            $data_based_array = json_decode($dataFromRedis, true);
            $conn_id = $data_based_array['id_conn'];
            /** @var ConnectionInterface $client */
            foreach ($this->clients as $client) {
                if ($client->resourceId === $conn_id) {
                    $client->send((json_encode([
                        'type' => 'service',
                        'sub_type' => 'notice',
                        'message' =>
                            'Вы открыли новое окно чата. Множественное открытие чата не доступно.
                            Проверьте последнее открытое вами окно данной страницы.',
                        'from' => 'server'
                    ])));
                    $this->detachClient($client);
                    $this->disconnectClient($client);
                }
            }
        }
    }

    /**
     * @param User $moderator
     * @param User $banedUser
     * @param string $description
     * @param int $duration
     * @return Ban
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveBanOrm(User $moderator, User $banedUser, string $description, int $duration)
    {
        $ban = new Ban();
        $current_date = new \DateTime();
        $ban->setModerator($moderator)
            ->setBannedUser($banedUser)
            ->setDescription($description);
        $endDate = $current_date->modify("+{$duration} hour");
        $ban->setEndDate($endDate);
        $this->entityManager->persist($ban);
        $this->entityManager->flush();
        return $ban;
    }

    /**
     * @param int $conn_id
     * @return null|object
     */
    private function findUserConnect(int $conn_id)
    {
        foreach ($this->clients as $client) {
            if ($client->resourceId === $conn_id) {
                return $client;
            }
        }
        return null;
    }

    /**
     * @param string $banedUsername
     * @param string $description
     * @param Ban $ban
     */
    private function sendMessageToBannedUser(string $banedUsername, string $description, Ban $ban, bool $isBan)
    {
        $dataFromRedis = $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $banedUsername);

        if (empty($dataFromRedis)) return;

        $data_based_array = json_decode($dataFromRedis, true);
        $conn_id = $data_based_array['id_conn'];
        $data_based_array['banned'] = $isBan;
        $this->redis->hset(self::REDIS_ONLINE_USERS_HASH, $banedUsername, json_encode($data_based_array));
        $userBanConnect = $this->findUserConnect($conn_id);

        if (!$userBanConnect) return;

        $date = new \DateTime();
        $diff = ($ban->getEndDate()->getTimestamp()) - ($date->getTimestamp());
        $userBanConnect->send(
            json_encode(
                [
                    'type' =>  $isBan ? 'ban' : 'unBan',
                    'duration' => $diff,
                    'description' => $description,
                    'from' => 'server'
                ]
            )
        );
    }

    /**
     * @param string $msg
     */
    private function sendAll(string $msg)
    {
        foreach ($this->clients as $client) {
            $client->send($msg);
        }
    }

    /**
     * @param array $arrayOfData
     * @param string $username
     * @param ConnectionInterface $conn
     */
    public function banUser(array $arrayOfData, string $username, ConnectionInterface $conn )
    {
        if (!isset($arrayOfData['description']) && !isset($arrayOfData['duration']) && !isset($arrayOfData['username'])) {
            $conn->send(EnumeratingChatMessages::getMsgWrongData());
            return;
        }

        $isBan = true;
        $typeBan = $arrayOfData['type'];
        $description = $arrayOfData['description'];
        $duration = $arrayOfData['duration'];
        $banedUsername = $arrayOfData['username'];
        $userRepository = $this->entityManager->getRepository(User::class);

        /** @var User $moderator */
        $moderator = $userRepository->findByUsername($username);
        /** @var User $banedUser */
        $banedUser = $userRepository->findByUsername($banedUsername);

        if (!$moderator || !$banedUser) {
            $conn->send(EnumeratingChatMessages::getMsgWrongData());
            return;
        }
        if (!$moderator->hasRole('ROLE_MODERATOR') || $banedUser->hasRole('ROLE_MODERATOR')) {
            $conn->send(EnumeratingChatMessages::getMsgAccessDenied());
            return;
        }
        $current_date = new \DateTime();
        /** @var mixed $lastBan */
        $lastBan = $this->entityManager->getRepository(Ban::class)->findLastBanByUserId($banedUser->getId());
        if ($lastBan && ($lastBan->getEndDate() > $current_date && $typeBan !== 'unBan')) {
            $conn->send(EnumeratingChatMessages::getMsgAlreadyBanned());
            return;
        }
        if($typeBan === 'unBan') $isBan = false;

        try {
            $ban = $this->saveBanOrm($moderator, $banedUser, $description, $duration);

            $conn->send(EnumeratingChatMessages::getMsgBanSuccess());
            $this->sendMessageToBannedUser($banedUsername, $description, $ban, $isBan);
            $msgAll = json_encode(array_merge(EnumeratingChatMessages::MSG_NOTICE, [
                'message' => "User {$banedUsername} is {$typeBan}ned",
                'clientUpdate' => [
                    'username' => $banedUsername,
                    'clientData' => json_decode(
                        $this->redis->hget(self::REDIS_ONLINE_USERS_HASH, $banedUsername),
                        true
                    )
                ]
            ]));
            $this->sendAll($msgAll);
        } catch (\Exception $e) {
            $msg_err = $e->getMessage();
            CustomLogger::_logNotice($msg_err, self::class);
            $conn->send(json_encode(array_merge(EnumeratingChatMessages::MSG_BAN_ERROR, ['message' => $msg_err])));
        }
    }
}