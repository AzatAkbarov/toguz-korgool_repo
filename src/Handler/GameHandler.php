<?php

namespace App\Handler;

use App\CustomLogger\CustomLogger;
use App\Entity\Gamelog;
use App\Entity\User;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GameHandler extends WebsocketHandler
{
    /** @var array $connectionsIdByUsername */
    private $connectionsIdToUsername = [];

    /** @var StepGameHandler $stepGameHandler */
    private $stepGameHandler;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->stepGameHandler = new StepGameHandler();
    }

    /**
     * @param string $key
     * @param string $username
     * @return bool
     */
    public function checkRoom(string $key, string $username)
    {
        $roomData = json_decode($this->redis->hget(self::REDIS_GAME_ROOMS_HASH, $key), true);

        if ($roomData && isset($roomData[$username])) return true;

        CustomLogger::_logNotice("Key room {$key} and username {$username} is not found", self::class);

        return false;
    }

    /**
     * @param string $key
     * @param string $username
     * @param ConnectionInterface $conn
     */
    public function addToRoom(string $key, string $username, ConnectionInterface $conn)
    {
        $color = json_decode($this->redis->hget(self::REDIS_GAME_ROOMS_HASH, $key), true)[$username]['side_color'];
        $this->connectionsIdToUsername[$conn->resourceId] = [
            'username' => $username,
            'keyRoom' => $key,
            'status' => null,
            'side_color' => $color,
            'point' => 0
        ];
        $this->rooms[$key][$username] = $conn;

        if (!$this->stepGameHandler->isStepsData($key)) $this->stepGameHandler->setBaseGameState($key);

        $conn->send(json_encode(['type' => 'service', 'status' => 'success']));
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function removeFromRoom(ConnectionInterface $conn)
    {
        $username = $this->getUsernameByConnection($conn);
        $keyRoom = $this->getRoomIdByConnection($conn);

        unset($this->rooms[$keyRoom][$username]);
        unset($this->connectionsIdToUsername[$conn->resourceId]);

        if ($this->rooms[$keyRoom]) {
            /** @var ConnectionInterface $connOpponent */
            $connOpponent = $this->rooms[$keyRoom][array_keys($this->rooms[$keyRoom])[0]];
            $connOpponent->send(json_encode(['type' => 'abortGame', 'message' => 'The enemy left this game']));
        }

        if (empty($this->rooms[$keyRoom])) {
            unset($this->rooms[$keyRoom]);
            $this->redis->hdel(self::REDIS_GAME_ROOMS_HASH, $keyRoom);
        }
    }

    /**
     * @param string $msg
     * @param ConnectionInterface $conn
     */
    public function messageProcessing(string $msg, ConnectionInterface $conn)
    {
        $msg = json_decode($msg, true);
        $keyRoom = $this->getRoomIdByConnection($conn);
        $sideColor = $this->getSideColorByConnection($conn);
        if ($msg['type'] === 'game_ready') {
            $opponentConnection = $this->getUserOpponentConnection($conn);
            if ($this->connectionsIdToUsername[$opponentConnection->resourceId]['status'] === 'visualization_ready') {
                $currentDate = new \DateTime();
                $gameStartDate = $currentDate->getTimestamp() + 5;
                $conn->send(
                    json_encode([
                        'type' => 'set_state',
                        'state_data' => array_merge(
                            ['Side' => $sideColor],
                            StepGameHandler::BASE_STATE_GAME
                        ),
                        'game_start_time' => $gameStartDate
                    ])
                );
                $opponentConnection->send(
                    json_encode([
                        'type' => 'set_state',
                        'state_data' => array_merge(
                            ['Side' => $this->getSideColorByConnection($opponentConnection)],
                            StepGameHandler::BASE_STATE_GAME
                        ),
                        'game_start_time' => $gameStartDate
                    ])
                );
                $this->connectionsIdToUsername[$conn->resourceId]['status'] = 'initializing_game';
                $this->connectionsIdToUsername[$opponentConnection->resourceId]['status'] = 'initializing_game';
            } else {
                $this->connectionsIdToUsername[$conn->resourceId]['status'] = 'visualization_ready';
            }
        } elseif ($msg['type'] === 'step') {
            if($this->stepGameHandler->isTurnSide($keyRoom, $sideColor)) {
                if($this->stepGameHandler->isNotEmptyCellAndNotSalt($keyRoom, $msg['cell_number'], $sideColor)) {
                    $this->stepProcessing($conn, $msg['cell_number'], $sideColor, $keyRoom);
                } else {
                    $conn->send(json_encode(['type' => 'click_again']));
                }
            }
        }
    }

    /**
     * @param ConnectionInterface $conn
     * @return ConnectionInterface
     */
    public function getUserOpponentConnection(ConnectionInterface $conn)
    {
        $room = $this->rooms[$this->connectionsIdToUsername[$conn->resourceId]['keyRoom']];
        unset($room[$this->getUsernameByConnection($conn)]);
        return $room[array_keys($room)[0]];
    }

    /**
     * @param ConnectionInterface $conn
     * @return string
     */
    public function getUsernameByConnection(ConnectionInterface $conn)
    {
        return $this->connectionsIdToUsername[$conn->resourceId]['username'];
    }

    /**
     * @param ConnectionInterface $conn
     * @return string
     */
    public function getRoomIdByConnection(ConnectionInterface $conn)
    {
        return $this->connectionsIdToUsername[$conn->resourceId]['keyRoom'];
    }

    /**
     * @param ConnectionInterface $conn
     * @return string
     */
    public function getSideColorByConnection(ConnectionInterface $conn)
    {
        return $this->connectionsIdToUsername[$conn->resourceId]['side_color'];
    }


    /**
     * @param ConnectionInterface $conn
     * @param int $cell
     * @param string $sideColor
     * @param string $keyRoom
     */
    public function stepProcessing(ConnectionInterface $conn, int $cell, string $sideColor, string $keyRoom)
    {
        $opponentConn = $this->getUserOpponentConnection($conn);
        $opponentColor = $this->getSideColorByConnection($opponentConn);
        $this->stepGameHandler->setSideTurn($keyRoom, $opponentColor);

        $msg = [
            'type' => 'step',
            'data_step' => $this->stepGameHandler->getDataStepGame($keyRoom, $cell, $sideColor, $opponentColor)
        ];

        $conn->send(json_encode(array_merge($msg, ['isBlockCells' => true])));
        $opponentConn->send(json_encode(array_merge($msg, ['isBlockCells' => false])));

        $opponentPoint = $this->stepGameHandler->getPointInCauldron($keyRoom, $opponentColor);
        $point = $this->stepGameHandler->getPointInCauldron($keyRoom, $sideColor);
        $isAtSyroo = $this->stepGameHandler->isAtSyroo($keyRoom, $opponentColor);

        $this->connectionsIdToUsername[$opponentConn->resourceId]['point'] = $opponentPoint;
        $this->connectionsIdToUsername[$conn->resourceId]['point'] = $point;

        try {
            if ($point >= 81 || $opponentPoint >= 81 || $isAtSyroo) $this->endGame($conn, $isAtSyroo);
        } catch (\Exception $e) {
            CustomLogger::_logNotice(
                "An error has occurred in method stepProcessing->endGame: {$e->getMessage()}",
                self::class
            );
        }
    }

    /**
     * @param ConnectionInterface $conn
     * @param bool $isAtSyroo
     * @param bool $isDisconnect
     */
    public function endGame(ConnectionInterface $conn, bool $isAtSyroo = false, bool $isDisconnect = false)
    {
        $statusGameWon = null;
        $draw = false;

        try {
            $opponentConn = $this->getUserOpponentConnection($conn);
            $keyRoom = $this->connectionsIdToUsername[$conn->resourceId]['keyRoom'];
            $point = $this->connectionsIdToUsername[$conn->resourceId]['point'];
            $opponentPoint = $this->connectionsIdToUsername[$opponentConn->resourceId]['point'];
        } catch (\Exception $e) {
            CustomLogger::_logNotice(
                "An error has occurred in method endGame->getData [isDisconnect => {$isDisconnect}]: {$e->getMessage()}",
                self::class
            );
        }

        try {
            if ($opponentPoint >= 82) {
                $statusGameWon = false;
                $winnerConnect = $opponentConn;
            } elseif ($point >= 82 || $isAtSyroo) {
                $statusGameWon = true;
                $winnerConnect = $conn;
            } elseif ($isDisconnect) $winnerConnect = $opponentConn;
            elseif ($point === 81 && $opponentPoint === 81) $draw = true;
        } catch (\Exception $e) {
            CustomLogger::_logNotice(
                "An error has occurred in method endGame->conditionWinner: {$e->getMessage()}",
                self::class
            );
        }

        if ($draw || $statusGameWon !== null || $isDisconnect) {
            try {
                if (!$isDisconnect) {
                    $conn->send(json_encode([
                        'type' => 'endGame',
                        'isBlockCells' => true,
                        'resultGame' => $draw ? 'draw' : $statusGameWon ? 'won' : 'lose'
                    ]));

                    $opponentConn->send(json_encode([
                        'type' => 'endGame',
                        'isBlockCells' => true,
                        'resultGame' => $draw ? 'draw' : $statusGameWon ? 'lose' : 'won'
                    ]));
                }
            } catch (\Exception $e) {
                CustomLogger::_logNotice(
                    "An error has occurred in method endGame->winnerHaveOrDisconnect->sendResultGame: {$e->getMessage()}",
                    self::class
                );
            }

            try {
                if ($point || $opponentPoint) $this->saveResultGame($keyRoom, $winnerConnect ?? null);
            } catch (\Exception $e) {
                CustomLogger::_logNotice(
                    "An error has occurred in method endGame->winnerHaveOrDisconnect->saveResultGame: {$e->getMessage()}",
                    self::class
                );
            }

            try {
                $this->stepGameHandler->clearGameData($keyRoom);
            } catch (\Exception $e) {
                CustomLogger::_logNotice(
                    "An error has occurred in method endGame->winnerHaveOrDisconnect->clearData: {$e->getMessage()}",
                    self::class
                );
            }

            try {
                $this->removeFromRoom($conn);
                $this->disconnectClient($conn);
            } catch (\Exception $e) {
                CustomLogger::_logNotice(
                    "An error has occurred in method endGame->winnerHaveOrDisconnect->closeConn: {$e->getMessage()}",
                    self::class
                );
            }

            try {
                $this->removeFromRoom($opponentConn);
                $this->disconnectClient($opponentConn);
            } catch (\Exception $e) {
                CustomLogger::_logNotice(
                    "An error has occurred in method endGame->winnerHaveOrDisconnect->closeOpponentConn: {$e->getMessage()}",
                    self::class
                );
            }
        }
    }


    /**
     * @param string $keyRoom
     * @param ConnectionInterface|null $winner
     */
    public function saveResultGame(string $keyRoom, ConnectionInterface $winner = null)
    {
        $entityManager = $this->entityManager;
        $gameLog = new Gamelog();

        try {
            if ($winner) {
                /** @var User $winner */
                $winner = $entityManager
                    ->getRepository(User::class)
                    ->findByUsername($this->getUsernameByConnection($winner));
            }
        } catch (\Exception $e) {
            CustomLogger::_logNotice(
                "An error has occurred in method saveResultGame->getUser: {$e->getMessage()}",
                self::class
            );
        }

        try {
            $gameLog
                ->setSteps(json_encode($this->stepGameHandler->getGameLogStepsData($keyRoom)))
                ->setWinner($winner)
                ->setPlayerBlack($this->getUserByColor($keyRoom, 'Black'))
                ->setPlayerWhite($this->getUserByColor($keyRoom, 'White'))
                ->setTotalPointBlack($this->stepGameHandler->getPointInCauldron($keyRoom, 'Black'))
                ->setTotalPointWhite($this->stepGameHandler->getPointInCauldron($keyRoom, 'White'));

            $entityManager->persist($gameLog);
            $entityManager->flush();
        } catch (\Exception $e) {
            CustomLogger::_logNotice(
                "An error has occurred in method saveResultGame->saveGameLog: {$e->getMessage()}",
                self::class
            );
        }
    }

    /**
     * @param string $keyRoom
     * @param string $color
     * @return null | User
     */
    public function getUserByColor(string $keyRoom, string $color)
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        foreach ($this->rooms[$keyRoom] as $conn) {
            if ($this->connectionsIdToUsername[$conn->resourceId]['side_color'] === $color) {
                return $userRepository->findByUsername($this->connectionsIdToUsername[$conn->resourceId]['username']);
            }
        }
        return null;
    }
}