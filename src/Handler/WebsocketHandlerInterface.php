<?php
/**
 * Created by PhpStorm.
 * User: kremnevea
 * Date: 07.09.18
 * Time: 13:59
 */

namespace App\Handler;

use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface WebsocketHandlerInterface
{
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);

    /**
     * @param string $token
     * @param ConnectionInterface $conn
     * @return mixed
     */
    public function getUserByToken(string $token, ConnectionInterface $conn);

    /**
     * @param ConnectionInterface $conn
     * @return void
     */
    public function disconnectClient(ConnectionInterface $conn);
}