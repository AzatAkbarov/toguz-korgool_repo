<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $users_data = [
            'emails' => [
                'test@gmail.com',
                'starticys@gmail.com',
                'akbarovazat25@gmail.com',
                'crocodiller227@gmail.com',
                'aitmat473@gmail.com',
                'almazov.urmat@gmail.com',
                'admin@gmail.com'
            ],
            'full_names' => [
                'test',
                'Кремнев Егор',
                'Акбаров Азат',
                'Степанов Дмитрий',
                'Моллали у. Айтмат',
                'Алмазов Урмат',
                'Admin'
            ]
        ];

        foreach ($users_data['emails'] as $key => $user_email) {
            $username = str_replace('@gmail.com', '', $user_email);
            $user = new User();
            $user
                ->setUsername($username)
                ->setEnabled(true)
                ->setFullName($users_data['full_names'][$key])
                ->setPlainPassword('root')
                ->setEmail($user_email);
            if ($users_data['full_names'][$key] === 'Admin') {
                $user->addRole('ROLE_ADMIN');
            }
            $manager->persist($user);
        }
        $manager->flush();
    }
}