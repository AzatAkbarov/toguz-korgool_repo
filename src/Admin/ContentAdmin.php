<?php

namespace App\Admin;

use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class)
                   ->add('text', TextareaType::class)
                   ->add('category', EntityType::class, [
                        'class' => Category::class
                   ])
                    ->add('publicationDate', DateType::class, [
                        'widget' => 'single_text',
                        'required' => false,
                        'attr' => ['style' => 'width: 200px'],
                    ])
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('_action',null,array(
            'actions' => array(
                'show'=> array(),
                'edit'=> array(),
                'delete'=> array()
            )
        ));
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('id')
             ->add('title')
             ->add('text')
            ->add('publicationDate')
        ;
    }

}
