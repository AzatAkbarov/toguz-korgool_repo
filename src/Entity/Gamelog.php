<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GamelogRepository")
 */
class Gamelog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $steps;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="gamelogs")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="victories")
     */
    private $winner;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="playerWhiteSide")
     * @ORM\JoinColumn(nullable=false)
     */
    private $playerWhite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="playerBlackSide")
     * @ORM\JoinColumn(nullable=false)
     */
    private $playerBlack;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPointWhite;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPointBlack;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSteps()
    {
        return $this->steps;
    }

    public function setSteps($steps): self
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    public function getWinner(): ?User
    {
        return $this->winner;
    }

    public function setWinner(?User $winner): self
    {
        $this->winner = $winner;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getPlayerWhite(): ?User
    {
        return $this->playerWhite;
    }

    public function setPlayerWhite(?User $playerWhite): self
    {
        $this->playerWhite = $playerWhite;

        return $this;
    }

    public function getPlayerBlack(): ?User
    {
        return $this->playerBlack;
    }

    public function setPlayerBlack(?User $playerBlack): self
    {
        $this->playerBlack = $playerBlack;

        return $this;
    }

    public function getTotalPointWhite(): ?int
    {
        return $this->totalPointWhite;
    }

    public function setTotalPointWhite(int $totalPointWhite): self
    {
        $this->totalPointWhite = $totalPointWhite;

        return $this;
    }

    public function getTotalPointBlack(): ?int
    {
        return $this->totalPointBlack;
    }

    public function setTotalPointBlack(int $totalPointBlack): self
    {
        $this->totalPointBlack = $totalPointBlack;

        return $this;
    }
}
