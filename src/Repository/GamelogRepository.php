<?php

namespace App\Repository;

use App\Entity\Gamelog;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Gamelog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gamelog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gamelog[]    findAll()
 * @method Gamelog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GamelogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Gamelog::class);
    }

    public function countAllRowsInGameLog(){
        try {
            return $this->createQueryBuilder('Gamelog')
                ->select('count(Gamelog.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            echo $e->getMessage();
        }
    }

    public function getGamelogsByOffset( int $offset, int $qty)
    {
        return $this->createQueryBuilder('gamelog')
            ->select('gamelog')
            ->orderBy('gamelog.createdAt', 'desc')
            ->setMaxResults($qty)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     */
    public function countWinner($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog.winner)')
                ->where('gamelog.winner =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function countLoss($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog.winner)')
                ->where('gamelog.playerWhite =:id')
                ->orWhere('gamelog.playerBlack =:id')
                ->andWhere('gamelog.winner !=:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function countDeadHeat($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.winner is null')
                ->andWhere('gamelog.playerBlack =:id or gamelog.playerWhite =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function countBlackSideWherePlayerWin($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.playerBlack =:id')
                ->andWhere('gamelog.winner =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function countWhiteSideWherePlayerWin($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.playerWhite =:id')
                ->andWhere('gamelog.winner =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    /**
     * @param $id
     * @return mixed
     */
    public function countBlackSideWherePlayerLoss($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.playerBlack =:id')
                ->andWhere('gamelog.winner !=:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function countWhiteSideWherePlayerLoss($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.playerWhite =:id')
                ->andWhere('gamelog.winner !=:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function countDeadHeatWhiteSide($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.winner is null')
                ->andWhere('gamelog.playerWhite =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function countDeadHeatBlackSide($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.winner is null')
                ->andWhere('gamelog.playerBlack =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    public function countAllPlayerGames($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('count(gamelog)')
                ->where('gamelog.playerWhite =:id')
                ->orWhere('gamelog.playerBlack =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function getUserGamelogsByOffset( int $offset, int $qty, User $user)
    {
        return $this->createQueryBuilder('gamelog')
            ->where('gamelog.playerWhite =:id')
            ->orWhere('gamelog.playerBlack =:id')
            ->orderBy('gamelog.createdAt', 'desc')
            ->setParameter('id', $user)
            ->setMaxResults($qty)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return mixed|null
     */
    public function countAllRowsInGameLogByUser(User $user){
        try {
            return $this->createQueryBuilder('Gamelog')
                ->select('count(Gamelog)')
                ->where('Gamelog.playerWhite =:id')
                ->orWhere('Gamelog.playerBlack =:id')
                ->setParameter('id', $user)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function sumTotalPointWhiteSide($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('sum(gamelog.totalPointWhite)')
                ->where('gamelog.playerWhite =:id')
                ->andWhere('gamelog.winner =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult()
            ;
        } catch (NonUniqueResultException $e) {
        }
    }

    public function sumTotalPointBlackSide($id){
        try {
            return $this->createQueryBuilder('gamelog')
                ->select('sum(gamelog.totalPointBlack)')
                ->where('gamelog.playerBlack =:id')
                ->andWhere('gamelog.winner =:id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult()
                ;
        } catch (NonUniqueResultException $e) {
        }
    }
}
