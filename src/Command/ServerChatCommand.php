<?php

namespace App\Command;

use App\CustomLogger\CustomLogger;
use App\Handler\WebsocketHandler;
use App\Service\ServerChat;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ServerChatCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('server-chat:start')
            // the short description shown while running "php bin/console list"
            ->setHelp("Starts the chat socket demo")
            // the full command description shown when running the command with
            ->setDescription('Starts the chat socket demo')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $redis = RedisAdapter::createConnection(WebsocketHandler::REDIS_DSN);
            $redis->flushAll();
            new CustomLogger($this->getContainer());
            CustomLogger::setPrefix('CHAT');
            file_put_contents($this->getContainer()->getParameter('logger_file_path'), '', FILE_APPEND | LOCK_EX);
            $port = $this->getContainer()->getParameter('server_chat')['port'];
            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        $this->getContainer()->get(ServerChat::class)
                    )
                ),
                $port
            );
            $output->writeln(sprintf('Launching a chat websocket server on port: %d', $port));
            $server->run();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}