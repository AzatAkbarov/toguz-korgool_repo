<?php
/**
 * Created by PhpStorm.
 * User: kremnevea
 * Date: 07.09.18
 * Time: 7:17
 */

namespace App\Command;

use App\CustomLogger\CustomLogger;
use App\Service\ServerGame;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServerGameCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('server-game:start')
            // the short description shown while running "php bin/console list"
            ->setHelp("Starts the game socket")
            // the full command description shown when running the command with
            ->setDescription('Starts the game socket')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            new CustomLogger($this->getContainer());
            CustomLogger::setPrefix('GAME');
            file_put_contents($this->getContainer()->getParameter('logger_file_path'), '', FILE_APPEND | LOCK_EX);
            $port = $this->getContainer()->getParameter('server_game')['port'];
            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        $this->getContainer()->get(ServerGame::class)
                    )
                ),
                $port
            );
            $output->writeln(sprintf('Launching a game websocket server on port: %d', $port));
            $server->run();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}