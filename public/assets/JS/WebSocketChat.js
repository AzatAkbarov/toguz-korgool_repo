function chatWork(urlChat, urlGame, clientUsername, token, pathToAssets, userRole) {
    /**
     * Note that you need to change the "sandbox" for the URL of your project.
     * According to the configuration in Sockets/ServerChat.phpat.php , change the port if you need to.
     * @type WebSocket
     */
    let conn = new WebSocket(`${urlChat}/chat?token=${token}`);
    let soundMessageBtn = true;
    let banDuration = 0;
    let usersOnline = {};
    let destination = 'room@all';
    let storageMsg = {};
    let timeoutInviteToGame = 0;
    let timerInvite = null;
    let inviteFrom = null;
    const delayNetwork = 1000;
    const durationInvite = 30000;
    const defaultAvatar = `${pathToAssets}/images/default/avatar_default.png`;

    conn.onopen = function (e) {
        console.info("Connection established succesfully");
    };

    parsData = (data) => {
        const tmp = JSON.parse(data);
        const {onlineClients} = tmp;
        return {
            ...tmp,
            onlineClients: onlineClients ? Object.keys(onlineClients || {}).reduce((acc, key) => {
                acc[key] = JSON.parse(onlineClients[key]);
                return acc;
            }, {}) : {}
        }
    };

    conn.onmessage = function (e) {
        const data = parsData(e.data);
        if (!$.isEmptyObject(data.onlineClients)) usersOnline = data.onlineClients;
        if (data.type === 'message') {
            let keyStorageMsg = data['destination'];
            if (keyStorageMsg === `user@${clientUsername}`) keyStorageMsg = `user@${data.from}`;
            if (storageMsg[keyStorageMsg] === undefined) storageMsg[keyStorageMsg] = [];
            storageMsg[keyStorageMsg].push({from: data.from, message: data.message});

            let pregUser = keyStorageMsg.replace(/user@/, '');
            if (pregUser !== clientUsername && destination !== keyStorageMsg) {
                $(`.noticeMsg[data-destination="${keyStorageMsg}"]`).html('<i class="fa fa-envelope"></i>');
            }

            if (data['from'] === clientUsername) {
                Chat.appendMessage([{from: data.from, message: data.message}]);
                return;
            }

            if (destination === keyStorageMsg) Chat.appendMessage([data]);
        } else if (data.type === 'service') Chat.processingServiceMessage(data);
        else if (data.type === 'ban') startBan(data.duration, data.description);
        else if (data.type === 'unBan') {
            banDuration = 0;
            showServiceMessage('Вас разбанили по причине: ' + data.description);
        } else if (data.type === 'invite') {
            inviteFrom = data.from_username;
            showModalInviteToGame(usersOnline[inviteFrom].showName, data.timeout);
        } else if(data.type === 'abort_invite') {
            showModalAboutAbortInvate(data.from)
        } else if (data.type === 'start_game') {
            gameProcessing(urlGame, token, data.game_room_id, conn);
        }
    };

    conn.onerror = function (e) {
        errorDisconnect();
        console.error(e);
    };

    conn.onclose = function (e) {
        errorDisconnect();
        console.error(e);
    };

    $("#form-message").keypress(function (event) {
        if (event.which === 13) sendForm();
    });

    $("#form-submit").click(function () {
        sendForm();
    });

    $(".soundMessageBtn").click(function () {
        let btnIcon = $(this).find('i');

        if (soundMessageBtn) {
            soundMessageBtn = false;
            btnIcon.attr('class', "fa fa-volume-off");
        } else {
            soundMessageBtn = true;
            btnIcon.attr('class', "fa fa-volume-up");
        }
    });

    $('body').on('click', '.ban', function () {
        let username = $(this).attr('data-username');
        banModalProcessing('#banModal', 'ban', username, usersOnline[username].showName);
    });

    $('body').on('click', '.unBan', function () {
        let username = $(this).attr('data-username');
        banModalProcessing('#unBanModal', 'unBan', username, usersOnline[username].showName);
    });

    $('body').on('click', '.name', function () {
        $('.item_block').hide();
        $(this).parent().find('.item_block').toggle();
    });

    $('body').on('click', '.sendTo', function () {
        $(`.messages ul`).html('');
        destination = $(this).attr('data-destination');
        let splitDestination = destination.split('@');
        let sendTo;
        if (destination === 'room@all') sendTo = 'Общий чат';
        if (splitDestination[0] !== 'room') {
            sendTo = usersOnline[splitDestination[1]].showName;
            $(`.noticeMsg[data-destination="${destination}"]`).html('');
        }
        $('.messages .title_chat span').html(sendTo);
        $('.item_block').hide();

        if (storageMsg[destination] && storageMsg[destination].length) {
            let arrayMsg = [];
            Object.keys(storageMsg[destination]).map((key) => {
                const savedMsg = storageMsg[destination][key];
                arrayMsg.push(savedMsg)
            });

            Chat.appendMessage(arrayMsg);
        }
    });

    $('body').on('click', '.inviteToGame', function () {
        if (timeoutInviteToGame !== 0) return;
        let toUsername = $(this).attr('data-username');
        $('.inviteToGame').hide();
        timeoutInviteToGame = getCurrentDateUtcInTimestamp() + durationInvite + delayNetwork;

        conn.send(JSON.stringify({
            type: 'invite',
            to_username: toUsername,
            timeout: timeoutInviteToGame - delayNetwork
        }));

        startTimeoutInviteToGame();
    });

    function showModalAboutAbortInvate(username) {
        let meBlock = $('.my_info .wrap span');
        timeoutInviteToGame = 0;
        meBlock.html('');
        swal({
            type: 'info',
            timer: 5000,
            title: `Пользователь ${username} отказался играть!`
        })
    }

    function showModalInviteToGame(showName, inviteTimeout) {
        if (timeoutInviteToGame !== 0) return;
        let timeout = inviteTimeout - getCurrentDateUtcInTimestamp();
        let timerInterval;
        swal({
            title: 'Приглашение в игру',
            html: `<hr><p>Пользователь ${showName} пригласил Вас сыграть с ним в партию</p><br><strong></strong>`,
            timer: timeout,
            showCancelButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Принять',
            cancelButtonText: 'Отказаться',
            onOpen: () => {
                timerInterval = setInterval(() => {
                    swal.getContent().querySelector('strong')
                        .textContent = Math.floor(swal.getTimerLeft() / 1000)
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.value) {
                conn.send(JSON.stringify({
                    type: 'inviteAnswer',
                    message: 'Пользователь принял ваше приглашение.',
                    answer: 1,
                    to_username: inviteFrom
                }));

                timerInvite && clearTimeout(timerInvite);
            } else {
                conn.send(JSON.stringify({
                    type: 'abortInvite',
                    message: 'Пользователь отклонил ваше приглашение.',
                    to_username: inviteFrom
                }));

                timerInvite && clearTimeout(timerInvite);
            }
        })
    }

    function startTimeoutInviteToGame() {
        let meBlock = $('.my_info .wrap span');
        meBlock.html('<div class="lds-dual-ring"></div>');
        let timer = setInterval(function () {
            if (getCurrentDateUtcInTimestamp() >= timeoutInviteToGame) {
                $('.inviteToGame').show();
                timeoutInviteToGame = 0;
                meBlock.html('');
                clearInterval(timer);
            }
        }, 100);
    }

    function getCurrentDateUtcInTimestamp() {
        let date = new Date();
        let now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());

        return new Date(now_utc).getTime();
    }

    function startBan(time, description) {
        let duration = parseInt(time, 10);
        let date = new Date();
        date.setSeconds(date.getSeconds() + duration);
        date = getFormatDate(date);
        showServiceMessage('Вас забанили по причине: ' + description + '</br>До ' + date);
        banDuration = duration * 1000;
        timeOutBan();
    }

    const getTwo = (val) => {
        return `0${val}`.slice(-2);
    };

    function getFormatDate(date) {
        return getTwo(date.getDate()) + '-' + getTwo(date.getMonth() + 1) + '-' +
            date.getFullYear() + ' ' + getTwo(date.getHours()) + ':' + getTwo(date.getMinutes());
    }
    function showBanModal(typeBan, recipientUsername, recipientShowName) {
        swal({
            title: `Инструментарий бана`,
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Забанить',
            cancelButtonText: 'Отмена',
            html:
                `<hr><p>Забанить пользователя ${recipientShowName}</p><div class="form-group">
                            <label class="col-form-label">Причина:</label>
                            <textarea id="swal-input1" required="true" class="form-control message-text" placeholder="Обязательно к заполнению!"></textarea>
                        </div><label>На срок в часах</label>`,
            input: 'range',
            inputAttributes: {
                min: 1,
                max: 48,
                step: 1
            },
            inputValue: 1,
            inputValidator: (value) => {
            },
            preConfirm: function () {
                let descriptionBlock = $('#swal-input1').val();
                return new Promise(function (resolve) {
                    if (descriptionBlock.length >= 3) {
                        let duration = $('.swal2-range > output').text();
                        conn.send(JSON.stringify({
                            type: 'ban',
                            username: recipientUsername,
                            duration: duration,
                            description: descriptionBlock
                        }));
                        resolve()
                    } else if(descriptionBlock.length === 0) {
                        swal.showValidationError(
                            'Укажите причину!'
                        )
                    } else {
                        swal.showValidationError(
                            'Причина не может быть столь короткой!'
                        )
                    }
                })
            },
            onOpen: function () {
                $('#swal-input1').focus();
            }
        })
    }
    function showUnBanModal(typeBan, recipientUsername, recipientShowName) {
        swal({
            title: `Инструментарий бана`,
            text: `Разбанить пользователя ${recipientShowName}`,
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Разбанить',
            cancelButtonText: 'Отмена',
            input: 'textarea',
            inputValidator: (value) => {
                return !value && 'Укажите причину!'
            },
            preConfirm: function () {
                let descriptionBlock = $('.swal2-textarea').val();
                console.log(descriptionBlock.length);
                return new Promise(function (resolve) {
                    if (descriptionBlock.length >= 3) {
                        conn.send(JSON.stringify({
                            type: typeBan,
                            username: recipientUsername,
                            duration: 0,
                            description: descriptionBlock
                        }));
                        resolve()
                    } else if(descriptionBlock.length === 0) {
                        swal.showValidationError(
                            'Укажите причину!'
                        )
                    } else {
                        swal.showValidationError(
                            'Причина не может быть столь короткой!'
                        )
                    }
                })
            },
            onOpen: function () {
                $('#swal-input1').focus();
            }
        })

    }

    function banModalProcessing(targetModal, typeBan, recipientUsername, recipientShowName) {
        if(targetModal === '#banModal') {
            showBanModal(typeBan, recipientUsername, recipientShowName)
        } else {
            showUnBanModal(typeBan, recipientUsername, recipientShowName)
        }
    }

    function errorDisconnect() {
        let frameBlock = $('#frame');

        if (!frameBlock.hasClass('disconnect')) {
            frameBlock.addClass('disconnect');
            usersOnline = {};
            $(`#sidepanel`).empty();
            $(`#form-message`).prop('disabled', true);
            showServiceMessage('Соединение с сервером потеряно.');
        }
    }

    function timeOutBan() {
        let timer = setInterval(function () {
            if (destination.split('@')[0] === 'room') $(`#form-message`).prop('disabled', true);
            else $(`#form-message`).prop('disabled', false);
            banDuration = banDuration - 100;
            if (banDuration <= 0) {
                clearInterval(timer);
                $(`#form-message`).prop('disabled', false);
            }
        }, 100);
    }

    function showServiceMessage(message) {
        const messageServerHtml = '<li class="replies">' +
            `<img src="${defaultAvatar}" alt="#" />` +
            '<span>Сервисное сообщение</span>' +
            `<p style="font-weight: bold">${message} </p>` +
            '</li>';

        $(`.messages ul`).append(messageServerHtml);
        soundNewMessage();
    }

    function soundNewMessage() {
        let audio = new Audio();
        audio.src = `${pathToAssets}/sounds/unsure.mp3`;
        audio.autoplay = true;
    }

    function sendForm() {
        if (destination.split('@')[0] === 'room' && banDuration !== 0) return;
        let msgInput = $("#form-message");
        let msg = msgInput.val();

        if ($.trim(msg) !== '') {
            Chat.sendMessage(msg);
            msgInput.val("");
        }

        return false;
    }

    function renderMe(me) {
        const myInfoHtml = '<div class="wrap" style="text-align: center">' +
            `<img class="my_avatar" src="${me.avatar}" alt="#" />` +
            `<p class="name">${me.showName}</p>` +
            '<span></span>' +
            '</div>';

        $('.my_info').html(myInfoHtml);
    }

    function setOptionModerator(ban, username) {
        let banClass = 'class="ban banOption"';
        let content = 'забанить пользователя';
        let targetModal = 'banModal';

        if (ban === true) {
            banClass = 'class="unBan banOption"';
            content = 'разбанить пользователя';
            targetModal = 'unBanModal';
        }

        return `<p ${banClass} data-username="${username}" data-toggle="modal"` +
            `data-target="#${targetModal}">` +
            `${content}` +
            `</p>`;
    }

    function updateClientGameStatus(client) {
        let username = client.username;
        usersOnline[username] = client.clientData;
        if (client.clientData.is_played === true) {
            if (username === clientUsername) {
                $('#contacts').addClass('game_status')
            } else {
                $(`.inviteToGame[data-username="${username}"]`).replaceWith(`<p class="in_game" data-username="${username}">играет</p>`)
            }
        } else {
            if (username === clientUsername) {
                $('#contacts').removeClass('game_status')
            } else {
                $(`.in_game[data-username="${username}"]`).replaceWith(`<p class="inviteToGame" data-username="${username}">пригласить в игру</p>`)
            }
        }
    }

    function updateClientData(client) {
        let username = client.username;
        usersOnline[username] = client.clientData;

        if (userRole === 'moderator') {
            let banStatus = client.clientData.banned;
            $(`.banOption[data-username="${username}"]`).replaceWith(setOptionModerator(banStatus, username));
        }
    }

    var Chat = {
        appendToClientsList: function (onlineList, role) {
            const userListHtml = Object.keys(onlineList).map((userName) => {
                let optionModerator = '';
                let playOption = `<p class="inviteToGame" data-username="${userName}">пригласить в игру</p>`;

                if (onlineList[userName].moderator !== true && role === 'moderator') {
                    optionModerator = setOptionModerator(onlineList[userName].banned, userName);
                }

                if (userName === clientUsername) {
                    renderMe(onlineList[userName]);
                    return '';
                }

                if (onlineList[userName].is_played) {
                    playOption = `<p class="in_game" data-username="${userName}">играет</p>`;
                }

                return '<li class="contact">' +
                    '<div class="wrap">' +
                    `<span class="sendTo noticeMsg" data-destination="user@${userName}"></span>` +
                    `<img src="${onlineList[userName].avatar}" alt="" />` +
                    '<div class="meta">' +
                    `<p class="name">${onlineList[userName].showName}</p>` +
                    '<div class="item_block" style="display: none">' +
                    `${playOption}` +
                    `<p class="sendTo" data-destination="user@${userName}" >отправить сообщение</p>` +
                    '<p>просмотреть профаил</p>' +
                    `${optionModerator}` +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
            }).join('');

            $('#contacts ul').html(userListHtml);
        },
        processingServiceMessage: function (data) {
            if (data.message === 'onlineClients') this.appendToClientsList(usersOnline, userRole);
            else if (data.sub_type === 'notice') {
                if (data.clientUpdate) updateClientData(data.clientUpdate);
                showServiceMessage(data.message);
            }
            else if (data.sub_type === 'in_game') {
                if (data.clientUpdate) updateClientGameStatus(data.clientUpdate);
            }
            else if (data.sub_type === 'ban' || data.sub_type === 'unBan') {
                if (data.status === 'success') showServiceMessage('Операция выполнена успешна');
                else if (data.status === 'failed') showServiceMessage('Операция не выполнена по причине: ' + data.message);
            }
        },
        appendMessage: (arrayMsg) => {
            const messageHtml = Object.keys(arrayMsg).map((msg) => {
                const dataMsg = arrayMsg[msg];

                let senToClass = 'class="sendTo"';
                let fromClass = 'replies';
                if (dataMsg.from === clientUsername) {
                    fromClass = 'sent';
                    senToClass = '';
                }

                return `<li class="${fromClass}">` +
                    `<span style="cursor:pointer;" ${senToClass} data-destination="user@${dataMsg.from}">` +
                    `${usersOnline[dataMsg.from].showName}` +
                    '</span>' +
                    `<img src="${usersOnline[dataMsg.from].avatar}" alt="#" />` +
                    `<p>${dataMsg.message}</p>` +
                    '</li>';
            }).join('');

            $(`.messages ul`).append(messageHtml);
            if (soundMessageBtn) soundNewMessage();
            let divMessages = $(".messages");
            divMessages.scrollTop(divMessages.get(0).scrollHeight);
        },
        sendMessage: function (text) {
            conn.send(JSON.stringify({type: 'message', message: text, destination: destination}));
        }
    };
}